import json
import logging
import re
from typing import Dict, List

import azure.functions as func
from bs4 import BeautifulSoup

TASK_PATTERN = re.compile(r'@([a-zA-Z0-9_\-\\.,]+): (.*)')


def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    tasks = extract_tasks(req.get_body().decode(), json.loads(req.params.get('user_map')))

    return func.HttpResponse(
        json.dumps(tasks),
        status_code=200,
        mimetype="application/json"
    )


def extract_users(userstring: str, user_map: Dict) -> str:
    users = userstring.split(',')
    user_principals = list()
    for user in users:
        try:
            user_principals.append(user_map[user])
        except KeyError:
            logging.info(f'User map does not provide an entry for user "{user}".')
    return ';'.join(user_principals)


def remove_special_characters(string: str, chars=None) -> str:
    if chars is None:
        chars = ["\n", "\r", "\t"]

    result = string
    for char in chars:
        result = result.replace(char, '')

    return result


def create_task(inner_html, user_map: Dict, status: str):
    match = TASK_PATTERN.match(inner_html[0])
    if not match:
        raise AttributeError('inner_html cannot be parsed as a task.')

    title = match.group(2)
    for char in ["\n", "\r", "\t"]:
        title = title.replace(char, "")

    task = {
        'status': status,
        'title': remove_special_characters(match.group(2)),
        'description': '',
        'assignedUsers': extract_users(match.group(1), user_map)
    }

    if len(inner_html) > 1 and str(inner_html[1]) == '<br/>':
        description = ''.join([str(x) for x in inner_html[2:]])
        task.update({
            'description': remove_special_characters(description)
        })

    return task


def extract_tasks(onenote_html: str, user_map: Dict) -> List:
    soup = BeautifulSoup(onenote_html, 'html.parser')

    tasks = list()

    task_searches = [
        {'data-tag': 'to-do', 'status': 'opened'},
        {'data-tag': 'to-do:completed', 'status': 'completed'}
    ]

    for task_search in task_searches:
        task_results = soup.find_all(attrs={'data-tag': task_search['data-tag']})
        for task_result in task_results:
            try:
                tasks.append(create_task(task_result.contents, user_map, task_search['status']))
            except AttributeError:
                logging.info(f'HTML content cannot be parsed as a task: "{task_result.text}"')

    return tasks
