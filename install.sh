#!/bin/bash

# Function app and storage account names must be unique.
storageName=onenoteutils
functionAppName=eog-onenote-utils
region=westeurope

resourceGroup=Stephan

# Create an Azure storage account in the resource group.
az storage account create \
  --name $storageName \
  --location $region \
  --resource-group $resourceGroup \
  --sku Standard_LRS

# Create a serverless function app in the resource group.
az functionapp create \
  --name $functionAppName \
  --storage-account $storageName \
  --consumption-plan-location $region \
  --resource-group $resourceGroup \
  --functions-version 3 \
  --os-type Linux \
  --runtime python \
  --runtime-version 3.8
