#
# Copyright 2021 Stephan Müller
#
# Licensed under the MIT license

import json
import os

import azure.functions as func

from tasks import extract_tasks, extract_users, main

__dirname__ = os.path.dirname(os.path.abspath(__file__))

USER_MAP = {'Stephan': 'stephan@example.com', 'Ole': 'ole@example.com'}


def test_main():
    req = func.HttpRequest(
        method='POST',
        body=b'<html lang="de-DE"></html>',
        url='/api/tasks',
        params={'user_map': json.dumps(USER_MAP)}
    )
    resp = main(req)
    assert resp.get_body().decode() == '[]'


def test_extract_users():
    users = extract_users("Ole,Stephan", USER_MAP)
    assert users == 'ole@example.com;stephan@example.com'


def test_extract_tasks():
    with open(os.path.join(__dirname__, "onenote_content.html"), "r") as file:
        onenote_content = file.read()
    tasks = extract_tasks(onenote_content, USER_MAP)

    assert tasks == [
        {
            'status': 'opened',
            'title': 'Clarify situation with water pump',
            'description': '',
            'assignedUsers': 'stephan@example.com'
        },
        {
            'status': 'opened',
            'title': 'Preparation of next meeting',
            'description': '',
            'assignedUsers': 'stephan@example.com;ole@example.com'
        },
        {
            'status': 'completed',
            'title': 'Construction of a solar plant',
            'description': '- Materials<br/>- Consumption',
            'assignedUsers': 'ole@example.com'
        }
    ]
